from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.decorators import login_required

from django.contrib.auth.models import User
from accounts.models import Profile
from django.db.utils import IntegrityError


def login_view(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user:
            login(request,user)
            return redirect('home')
        else:
            return render(request, 'core/home.html', {'error':'Error usuario o contraseña ingresados no son correctos'})
    return render(request,'core/home.html')

@login_required
def logout_view(request):
    logout(request)
    return redirect('home')
