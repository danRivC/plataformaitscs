from django.contrib import admin
from accounts.models import *

class ProfileAdmin(admin.ModelAdmin):
    readonly_fields = ('created', 'updated')
admin.site.register(Profile, ProfileAdmin)