from django.db import models
from django.contrib.auth.models import User
from ckeditor.fields import RichTextField

class Category(models.Model):
    name = models.CharField(max_length=255, verbose_name='Nombre Categoria')
    created = models.DateTimeField(auto_now_add=True, verbose_name='Fecha de creacion')
    updated = models.DateTimeField(auto_now=True, verbose_name='Fecha de modificacion')
    class Meta:
        verbose_name = 'categoria'
        verbose_name_plural = 'categorias'
        ordering = ['-created']

    def __str__(self):
        return self.name


class Carrer(models.Model):
    title = models.CharField(max_length=100, verbose_name='Titulo de Carrera')
    description = models.TextField(verbose_name='Descripción de la Carrera')
    image = models.ImageField(upload_to='carrers', verbose_name='Imagen Representativa')
    created = models.DateTimeField(auto_now_add=True, verbose_name='Fecha de creacion')
    updated = models.DateTimeField(auto_now=True, verbose_name='Fecha de modificacion')
    category = models.ForeignKey(Category, on_delete=models.CASCADE, verbose_name='Categoria')

    class Meta:
        verbose_name = 'carrera'
        verbose_name_plural = 'carreras'
        ordering = ['-created']

    def __str__(self):
        return self.title

class InformationCarrer(models.Model):
    requirements = RichTextField(verbose_name='Requerimientos')
    costos = RichTextField(verbose_name='Costos')
    carrer = models.OneToOneField(Carrer, on_delete=models.CASCADE)
    student_profile = RichTextField(verbose_name='Perfil del estudiante')
    created = models.DateTimeField(auto_now_add=True, verbose_name='Fecha de creacion')
    updated = models.DateTimeField(auto_now=True, verbose_name='Fecha de modificacion')

    class Meta:
        verbose_name = 'Informacion de la carrera'
        verbose_name_plural = 'Informacion sobre las carreras'
        ordering = ['-created']

    def __str__(self):
        return self.carrer.title

class Semester(models.Model):
    number_semester = models.IntegerField(verbose_name="Numero de Semestre")
    semester_name = models.CharField(max_length=240, verbose_name="Semestre")
    created = models.DateTimeField(auto_now_add=True, verbose_name='Fecha de creacion')
    updated = models.DateTimeField(auto_now=True, verbose_name='Fecha de modificacion')

    class Meta:
        verbose_name = 'Semestre'
        verbose_name_plural = 'Semestres'
        ordering = ['number_semester']

    def __str__(self):
        return self.semester_name

class Assessment(models.Model):
    assesment_name = models.CharField(max_length=255, verbose_name='Evaluaciones')
    assesment_description = RichTextField(verbose_name='Evaluaciones')
    created = models.DateTimeField(auto_now_add=True, verbose_name='Fecha de creacion')
    updated = models.DateTimeField(auto_now=True, verbose_name='Fecha de modificacion')

    class Meta:
        verbose_name = 'Evaluacion'
        verbose_name_plural = 'Evaluaciones'
        ordering = ['-created']

    def __str__(self):
        return self.assesment_name

class Asignature(models.Model):
    carrer = models.ManyToManyField(Carrer, verbose_name='Carrera', related_name='get_carrer')
    name_assignature = models.CharField(max_length=255, verbose_name='Materia')
    cicle = models.ForeignKey(Semester, on_delete=models.CASCADE, verbose_name='Semestre Asignado')
    evaluation = models.ManyToManyField(Assessment, verbose_name='Evaluacion', related_name='get_evaluation')
    created = models.DateTimeField(auto_now_add=True, verbose_name='Fecha de creacion')
    updated = models.DateTimeField(auto_now=True, verbose_name='Fecha de modificacion')

    class Meta:
        verbose_name = 'Materia'
        verbose_name_plural = 'Materias'
        ordering = ['-created']

    def __str__(self):
        return self.name_assignature


