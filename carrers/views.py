from django.shortcuts import render, get_object_or_404
from .models import Carrer, Category, InformationCarrer, Asignature, Semester, Assessment
from comments.models import Comment
from comments.forms import CommentForm
from django.views.generic import DetailView, ListView

"""
def course_detail(request, carrer_id):
    carrer = get_object_or_404(Carrer, id=carrer_id)
    carrers = Carrer.objects.all()
    informations = InformationCarrer.objects.all()
    assignatures = Asignature.objects.all()
    semesters = Semester.objects.all()
    for info in informations:
        if info.carrer.title == carrer.title:
            return render(request, 'carrers/course-details.html',
                            {'carrer': carrer,
                            'information': info,
                            'assignatures': assignatures,
                            'semesters': semesters,
                             
                            })

    return render(request, 'carrers/course-details.html', {'carrer':carrer, 'carrers':carrers})
"""

class CarrerDetailView(DetailView):
    model = Carrer


    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        informations = InformationCarrer.objects.all()
        context['asignatures'] = Asignature.objects.all()
        context['semesters'] = Semester.objects.all()
        context['assesments']= Assessment.objects.all()
        context['carrers'] = Carrer.objects.all()
        return context


class AlCarrerView(ListView):
    model = Carrer
    paginate_by = 30
    template_name = 'carrers/all-courses.html'

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['carrers'] = Carrer.objects.all()
        return context


