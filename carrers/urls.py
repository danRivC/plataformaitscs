from django.urls import path
from .views import CarrerDetailView, AlCarrerView

urlpatterns = [

    path('<int:pk>/<slug:slug>/', CarrerDetailView.as_view(), name='courseDetail'),
    path('all-courses/', AlCarrerView.as_view(), name="allCarrers")

]
"""
path('<int:carrer_id>/', views_carrers.course_detail, name='course_detail'),
"""