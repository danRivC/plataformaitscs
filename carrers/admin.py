from django.contrib import admin
from .models import *
# Register your models here.
class CarrerAdmin(admin.ModelAdmin):
    readonly_fields = ('created', 'updated')
admin.site.register(Carrer, CarrerAdmin)

class CategoryAdmin(admin.ModelAdmin):
    readonly_fields = ('created', 'updated')

admin.site.register(Category, CategoryAdmin)

class InformationCarrerAdmin(admin.ModelAdmin):
    readonly_fields = ('created', 'updated')

class AssignatureAdmin(admin.ModelAdmin):
    readonly_fields = ('created', 'updated')




admin.site.register(InformationCarrer, InformationCarrerAdmin)
admin.site.register(Asignature, AssignatureAdmin)
admin.site.register(Semester)
admin.site.register(Assessment)