from django.db import models
from django.conf import settings


class Comment(models.Model):
    created = models.DateTimeField(auto_now_add=True, verbose_name='Fecha de creacion')
    updated = models.DateTimeField(auto_now=True, verbose_name='Fecha de modificacion')
    content = models.TextField()
    author = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name='comments_author'
    )
    carrer = models.ForeignKey(
        'carrers.Carrer',
        on_delete=models.CASCADE,
        related_name='comments'

    )

    class Meta:
        verbose_name = 'Comentario'
        verbose_name_plural = 'Comentarios'
        ordering = ['-created']

    def __str__(self):
        return self.carrer.title
