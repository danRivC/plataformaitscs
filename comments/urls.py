from django.urls import path
from .views import create_comment

urlpatterns = [
    path('', create_comment, name='create comment'),
]