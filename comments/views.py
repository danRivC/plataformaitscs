from .forms import CommentForm
from django.shortcuts import redirect, render
from django.http import HttpResponseBadRequest
from django.urls import reverse_lazy

def create_comment(request):
    form = CommentForm(request.POST)
    if form.is_valid():
        comment = form.save(commit=False)
        comment.author = request.user
        comment.content = request.POST('content')
        comment.save()
        return redirect('home')
    return HttpResponseBadRequest()