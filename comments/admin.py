from django.contrib import admin
from .models import *

class CommentAdmin(admin.ModelAdmin):
    readonly_fields = ('created', 'updated')

admin.site.register(Comment, CommentAdmin)