from django.contrib import admin
from .models import Research

class ResearchAdmin(admin.ModelAdmin):
    readonly_fields = ['created', 'updated']

admin.site.register(Research, ResearchAdmin)