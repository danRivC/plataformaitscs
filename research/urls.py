from django.urls import path
from .views import ResearchesViews, ResearchDetail

urlpatterns=[
    path('researches/', ResearchesViews.as_view(), name='researches'),
    path('researches/<int:pk>/<slug:slug>/', ResearchDetail.as_view(), name='research_detail'),
]